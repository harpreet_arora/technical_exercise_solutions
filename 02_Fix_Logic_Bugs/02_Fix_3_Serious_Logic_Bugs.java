// Fixing three serious logic bugs
// The LOGIC BUGS corrected  can be seen below with comments / explanation of the change

public class Atlassian {
	
	/**
	 * Adds a watcher to all of the supplied issues.
	 * If there is partial success, the issues which we can modify will
	 * be modified and the ones we cannot will be returned in an ArrayList.
	 * @param issues the list of issues to update
	 * @param currentUser the user to run the operation as
	 * @param watcher the watcher to add to the issues
	 * @return an ArrayList<Issue> containing the issues that could not be modified
	*/
	public ArrayList<Issue> addWatcherToAll (final ArrayList<Issue> issues, final User currentUser, final User watcher) 
		{
		 
			 ArrayList<Issue> successfulIssues = new ArrayList<Issue> ();
			 ArrayList<Issue> failedIssues = new ArrayList<Issue> ();
			 for (Issue issue : issues) {
			 if (canWatchIssue(issue, currentUser, watcher)) {
			 successfulIssues.add (issue);
			 }
			 else {
			 failedIssues.add (issue);
			 }
			 }
			 
			 // LOGIC BUG 1:
			 // IF the successfulIssues list is NOT empty, ONLY THEN we would like to start watching issues
			 // NEGATION operator needs to be added in the IF condition
			 // commenting the incorrect line below
			 //if (successfulIssues.isEmpty()) {
			 if (!successfulIssues.isEmpty()) {
				 // LOGIC BUG 2:
				 // a watcher can watch issues, the currentUser is being passed as a parameter for startWatching method which is not right
				 // commenting the below incorrect line and passing the watcher instead
				 //watcherManager.startWatching (currentUser, successfulIssues);
				 watcherManager.startWatching (watcher, successfulIssues);
			 }
			 return failedIssues;
		}
		
	private boolean canWatchIssue (Issue issue, User currentUser, User watcher) {
	 if (currentUser.equals(watcher) || currentUser.getHasPermissionToModifyWatchers()) {
	 return issue.getWatchingAllowed (watcher);
	 }
	 // LOGIC BUG 3:
	 // function is set to return true if the above condition is false, the function will incorrectly return true if current user
	 // is NOT watcher and if current user does not have permission to modify watcher
	 // the function should false by default - commenting the incorrect line below:
	 // return true;
	 return false;
	}

}