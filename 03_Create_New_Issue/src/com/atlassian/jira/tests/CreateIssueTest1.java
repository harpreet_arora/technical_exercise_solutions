/**
 * This is a JUnit test that performs login, fills the "Create Issue" form using the Issue object that is 
 * passed to the submitIssue page 
 * 
 */

package com.atlassian.jira.tests;

import static org.junit.Assert.*;

import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.junit.*;

import com.atlassian.jira.pages.CreateNewIssuePage;
import com.atlassian.jira.pages.LoginPage;
import com.atlassian.jira.pages.Issue;
import com.atlassian.jira.pages.IssueDetailsPage;
import com.atlassian.jira.pages.SystemDashboardPage;

public class CreateIssueTest1 {
	
	@Test
	public void testCreateIssue()
	{	
		WebDriver driver = new FirefoxDriver();
		
		// Login to the page
		LoginPage dashLoginPage = PageFactory.initElements(driver, LoginPage.class);
		SystemDashboardPage sysDashPage = dashLoginPage.login("testacc17181@gmail.com", "Atlassian123@");
		
		// Create the issue (fill the Create Issue page/popup)
		CreateNewIssuePage createNewIssuePopUp= sysDashPage.createIssue();
		Issue issue = new Issue();
		issue.setProject("A Test Project");
		issue.setIssueType("Bug");
		issue.setSummary("This is a test bug being reported.");
		issue.setSecurityLevel("None");
		issue.setPriority("Minor");
		issue.setDueDate("23/Apr/15");
		issue.setComponents("Component 1");
		issue.setAffectsVersions("Version 2.0");
		issue.setFixVersions("Version 2.0");
		issue.setAssignee("Unassigned");
		issue.setEnvironment("This is a test description for the environment");
		issue.setDescription("This is test detailed description. This bug is being created to test the \"create new issue\" fuctionality in JIRA");
		issue.setRandomText("Test Text");
		issue.setVendorDeliveryDate("28/Apr/15");
		issue.setVendor("Oracle");
		issue.setOptLettuce(true);
		issue.setOptChilli(false);
		issue.setOptOnion(true);
		issue.setOptTomato(false);
		issue.setOriginalEstimate("4w");
		issue.setAnimal1("Fish");
		issue.setAnimal2("Shark");
		issue.setTester("");
		issue.setMySingleVersionPicker("Version 2.0");
		issue.setLabels("red");
		issue.setStoryPoints(331);
		issue.setSprint("35mm Capture - 2.7.1");
		issue.setEpicLink("Nages Test ");
		issue.setRegularExpression("NewIssue*");
		
		// Issue Details Page is returned when issue is submitted successfully
		IssueDetailsPage issueDetailsPage = createNewIssuePopUp.submitIssue(issue);
		
		// Verifying few of the WebElements on the issue details page
		
		//verify title of the page
		assertTrue("Title", Pattern.matches("\\[TST-\\d*\\] This is a test bug being reported\\. - Atlassian JIRA", issueDetailsPage.getTitle()));
		
		//verify few contents on the issue details page
		assertEquals("Summary", issue.getSummary(), issueDetailsPage.getSummary());
		assertEquals("Environment", issue.getEnvironment(), issueDetailsPage.getEnvironment());
		assertEquals("Issue Type", issue.getIssueType(), issueDetailsPage.getIssueType());
		assertEquals("Random Text", issue.getRandomText(), issueDetailsPage.getRandomText());
		assertEquals("Regular Expression", issue.getRegularExpression(), issueDetailsPage.getRegularExpression());
		assertEquals("Vendor", issue.getVendor(), issueDetailsPage.getVendor());
		assertEquals("Description", issue.getDescription(), issueDetailsPage.getDescription());
		
	}
}
