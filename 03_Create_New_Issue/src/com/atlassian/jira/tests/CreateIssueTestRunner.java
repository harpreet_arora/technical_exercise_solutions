/**
 * This is a JUnit test runner which runs the CreateIssueTest1 test that logs in, creates the issue
 * and validates that an issue is created successfully
 * 
 */

package com.atlassian.jira.tests;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class CreateIssueTestRunner {

	  public static void main(String[] args) {
		    Result result = JUnitCore.runClasses(CreateIssueTest1.class);
		    for (Failure failure : result.getFailures()) {
		      System.out.println(failure.toString());
		    }
	  }
}
