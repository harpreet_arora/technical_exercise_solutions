/**
 * The SystemDashboardPage PageObject has the button/event that can be invoked to have the 
 * Create New Issue popup
 */

package com.atlassian.jira.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.*;

public class SystemDashboardPage {
	
	WebDriver driver;
	
	@FindBy(how=How.LINK_TEXT, using="Create Issue")
	WebElement createIssue;
	
	public SystemDashboardPage(WebDriver driver) throws InterruptedException
	{
		this.driver = driver;
		driver.get("https://jira.atlassian.com/secure/Dashboard.jspa");
		 driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
	}
	
	// Click Create New Issue (pressing "c" opens the Create New Issue popup)
	public CreateNewIssuePage createIssue()
	{
		new Actions(driver).sendKeys("c").perform();
		return PageFactory.initElements(driver, CreateNewIssuePage.class);

	}
}
