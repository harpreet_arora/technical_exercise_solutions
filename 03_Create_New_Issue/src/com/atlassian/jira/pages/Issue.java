/**
 * The issue class has the state variables for Issue required to create the issue using JIRA
 * The test passes issue object to the CreateNewIssue page to create a new issue
 */

package com.atlassian.jira.pages;

public class Issue {
		
	private String project;
	private String issueType;
	private String summary;
	private String securityLevel;
	private String priority;
	private String dueDate;
	private String components;
	private String affectsVersions;
	private String fixVersions;
	private String assignee;
	private String environment;
	private String description;
	private String randomText;
	private String vendorDeliveryDate;
	private String vendor;
	private boolean optLettuce;
	private boolean optTomato;
	private boolean optOnion;
	private boolean optChilli;
	private String originalEstimate;
	private String animal1;
	private String animal2;
	private String tester;
	private String mySingleVersionPicker;
	private String labels;
	private int storyPoints;;
	private String sprint;	
	private String epicLink;
	private String regularExpression;
	
	public String getProject() {
		return project;
	}
	
	public void setProject(String project) {
		this.project = project;
	}
	public String getIssueType() {
		return issueType;
	}
	public void setIssueType(String issueType) {
		this.issueType = issueType;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getSecurityLevel() {
		return securityLevel;
	}
	public void setSecurityLevel(String securityLevel) {
		this.securityLevel = securityLevel;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getComponents() {
		return components;
	}
	public void setComponents(String components) {
		this.components = components;
	}
	public String getAffectsVersions() {
		return affectsVersions;
	}
	public void setAffectsVersions(String affectsVersions) {
		this.affectsVersions = affectsVersions;
	}
	public String getFixVersions() {
		return fixVersions;
	}
	public void setFixVersions(String fixVersions) {
		this.fixVersions = fixVersions;
	}
	public String getAssignee() {
		return assignee;
	}
	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
	public String getEnvironment() {
		return environment;
	}
	public void setEnvironment(String environment) {
		this.environment = environment;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getRandomText() {
		return randomText;
	}
	public void setRandomText(String randomText) {
		this.randomText = randomText;
	}
	public String getVendorDeliveryDate() {
		return vendorDeliveryDate;
	}
	public void setVendorDeliveryDate(String vendorDeliveryDate) {
		this.vendorDeliveryDate = vendorDeliveryDate;
	}
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	public boolean getOptLettuce() {
		return optLettuce;
	}
	public void setOptLettuce(boolean optLettuce) {
		this.optLettuce = optLettuce;
	}
	public boolean getOptTomato() {
		return optTomato;
	}
	public void setOptTomato(boolean optTomato) {
		this.optTomato = optTomato;
	}
	public boolean getOptOnion() {
		return optOnion;
	}
	public void setOptOnion(boolean optOnion) {
		this.optOnion = optOnion;
	}
	public boolean getOptChilli() {
		return optChilli;
	}
	public void setOptChilli(boolean optChilli) {
		this.optChilli = optChilli;
	}
	public String getOriginalEstimate() {
		return originalEstimate;
	}
	public void setOriginalEstimate(String originalEstimate) {
		this.originalEstimate = originalEstimate;
	}
	public String getAnimal1() {
		return animal1;
	}
	public void setAnimal1(String animal1) {
		this.animal1 = animal1;
	}
	public String getAnimal2() {
		return animal2;
	}
	public void setAnimal2(String animal2) {
		this.animal2 = animal2;
	}
	public String getTester() {
		return tester;
	}
	public void setTester(String tester) {
		this.tester = tester;
	}
	public String getMySingleVersionPicker() {
		return mySingleVersionPicker;
	}
	public void setMySingleVersionPicker(String mySingleVersionPicker) {
		this.mySingleVersionPicker = mySingleVersionPicker;
	}
	public String getLabels() {
		return labels;
	}
	public void setLabels(String labels) {
		this.labels = labels;
	}
	public int getStoryPoints() {
		return storyPoints;
	}
	public void setStoryPoints(int storyPoints) {
		this.storyPoints = storyPoints;
	}
	public String getSprint() {
		return sprint;
	}
	public void setSprint(String sprint) {
		this.sprint = sprint;
	}
	public String getEpicLink() {
		return epicLink;
	}
	public void setEpicLink(String epicLink) {
		this.epicLink = epicLink;
	}
	public String getRegularExpression() {
		return regularExpression;
	}
	public void setRegularExpression(String regularExpression) {
		this.regularExpression = regularExpression;
	}

}
