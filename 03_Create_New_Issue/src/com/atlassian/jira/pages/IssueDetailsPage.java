/**
 * The IssueDetails page is the page that shows details of the submitted issue
 * Few key elements are being returned from this page to validate if the issue was submitted successfully
 * 
 */

package com.atlassian.jira.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class IssueDetailsPage {
	
	WebDriver driver;
	
	@FindBy(how=How.ID, using="summary-val")
	WebElement summary;
	
	@FindBy(how=How.ID, using="environment-val")
	WebElement environment;

	@FindBy(how=How.ID, using="customfield_10040-val")
	WebElement vendor;
	
	@FindBy(how=How.ID, using="type-val")
	WebElement issueType;
	
	@FindBy(how=How.ID, using="customfield_10010-val")
	WebElement randomText;
	
	@FindBy(how=How.ID, using="customfield_14130-val")
	WebElement regularExpression;
	
	@FindBy(how=How.ID, using="description-val")
	WebElement description;
	
	public IssueDetailsPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public String getTitle()
	{
		return driver.getTitle();
	}
	
	public String getSummary()
	{
		return summary.getText();
	}
	
	public String getEnvironment()
	{
		return environment.getText();
	}
	
	public String getVendor()
	{
		return vendor.getText();
	}
	
	public String getIssueType()
	
	{
		return issueType.getText();
	}
	
	public String getRandomText()
	{
		return randomText.getText();
	}
	
	public String getRegularExpression()
	{
		return regularExpression.getText();
	}
	
	public String getDescription()
	{
		return description.getText();
	}
}
