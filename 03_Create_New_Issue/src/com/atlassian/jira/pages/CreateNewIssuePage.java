/**
 * The CreateNewIssuePage PageObject fills the "Create Issue" form using the Issue object that is 
 * passed to the submitIssue page 
 * 
 */

package com.atlassian.jira.pages;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.*;
import org.openqa.selenium.support.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreateNewIssuePage {
	
	WebDriver driver;
	
	// WebElements on the page
	
	@FindBy(how=How.ID, using="project-field")
	WebElement project;
	
	@FindBy(how=How.ID, using="issuetype-field")
	WebElement issueType;
	
	@FindBy(how=How.ID, using="summary")
	WebElement summary;
	
	@FindBy(how=How.ID, using="security")
	WebElement securityLevel;
	
	@FindBy(how=How.ID, using="priority-field")
	WebElement priority;
	
	@FindBy(how=How.ID, using="duedate")
	WebElement dueDate;
	
	@FindBy(how=How.ID, using="components-textarea")
	WebElement components;
	
	@FindBy(how=How.ID, using="versions-textarea")
	WebElement affectsVersions;
	
	@FindBy(how=How.ID, using="fixVersions-textarea")
	WebElement fixVersions;
	
	@FindBy(how=How.ID, using="assignee-field")
	WebElement assignee;
	
	@FindBy(how=How.ID, using="environment")
	WebElement environment;
	
	@FindBy(how=How.ID, using="description")
	WebElement description;
	
	@FindBy(how=How.ID, using="customfield_10010")
	WebElement randomText;
	
	@FindBy(how=How.ID, using="customfield_10041")
	WebElement vendorDeliveryDate;
	
	@FindBy(how=How.ID, using="customfield_10040")
	WebElement vendor;
	
	@FindBy(how=How.ID, using="customfield_10064-1")
	WebElement lettuce;
	
	@FindBy(how=How.ID, using="customfield_10064-2")
	WebElement tomato;
	
	@FindBy(how=How.ID, using="customfield_10064-3")
	WebElement onion;
	
	@FindBy(how=How.ID, using="customfield_10064-4")
	WebElement chilli;

	@FindBy(how=How.ID, using="timetracking")
	WebElement originalEstimate;
	
	@FindBy(how=How.ID, using="customfield_10061")
	WebElement animal1;
	
	@FindBy(how=How.ID, using="customfield_10061:1")
	WebElement animal2;
	
	@FindBy(how=How.ID, using="customfield_10530")
	WebElement tester;
	
	@FindBy(how=How.ID, using="customfield_10550")
	WebElement mySingleVersionPicker;
	
	@FindBy(how=How.ID, using="labels-textarea")
	WebElement labels;
	
	@FindBy(how=How.ID, using="customfield_10653")
	WebElement storyPoints;
	
	@FindBy(how=How.ID, using="customfield_11930-field")
	WebElement sprint;	
	
	@FindBy(how=How.ID, using="customfield_12931-field")
	WebElement epicLink;
	
	@FindBy(how=How.ID, using="customfield_14130")
	WebElement regularExpression;
		
	@FindBy(how=How.ID, using="create-issue-submit")
	WebElement createIssueSubmit;
	
	@FindBy(how=How.LINK_TEXT, using="Cancel")
	WebElement cancel;
	
	@FindBy(how=How.CLASS_NAME, using="issue-created-key issue-link")
	WebElement createdIssueLink;
		
	// Constructor
	public CreateNewIssuePage(WebDriver driver) {
		this.driver=driver;
	}
	
	
	// Method that submits/creates the issue
	public IssueDetailsPage submitIssue(Issue issue)
	{
		// the create new issue window appears as a popup
		//hence it will be required to go from parent window to the popup window
		String parentWindowHandler = driver.getWindowHandle();
		String subWindowHandler = null;

		// get all window handles
		Set<String> handles = driver.getWindowHandles(); 
		Iterator<String> iterator = handles.iterator();
		
		// loop and go to last handle, that will be the popup window
		while (iterator.hasNext()){
		    subWindowHandler = iterator.next();
		}
		
		driver.switchTo().window(subWindowHandler); // switch to popup window
		                                            // perform operations on popup
		
		// The create new issue window takes 1 to 3 seconds to open, giving it a 3 second wait time
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		project.sendKeys(issue.getProject());
		project.sendKeys(Keys.TAB);
		
		// The issueType field is disabled for 1-2 seconds when the project is being populated
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		issueType.clear();
		issueType.sendKeys(issue.getIssueType());
		project.sendKeys(Keys.TAB);
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		// perform operations on popup
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		summary.sendKeys(issue.getSummary());
		securityLevel.sendKeys(issue.getSecurityLevel());
		priority.sendKeys(issue.getPriority());
		
		dueDate.sendKeys(issue.getDueDate());
		components.sendKeys(issue.getComponents());
		affectsVersions.sendKeys(issue.getAffectsVersions());
		fixVersions.sendKeys(issue.getFixVersions());
		
		assignee.clear();
		assignee.sendKeys(issue.getAssignee());
		
		environment.sendKeys(issue.getEnvironment());
		description.sendKeys(issue.getDescription());
		
		randomText.clear();
		randomText.sendKeys(issue.getRandomText());
		
		vendorDeliveryDate.sendKeys(issue.getVendorDeliveryDate());
		vendor.sendKeys(issue.getVendor());
		
		originalEstimate.sendKeys(issue.getOriginalEstimate());
		animal1.sendKeys(issue.getAnimal1());
		animal2.sendKeys(issue.getAnimal2());
		tester.sendKeys(issue.getTester());
		mySingleVersionPicker.sendKeys(issue.getMySingleVersionPicker());
		labels.sendKeys(issue.getLabels());
		storyPoints.sendKeys(Integer.toString(issue.getStoryPoints()));
		sprint.sendKeys(issue.getSprint());
		
		epicLink.sendKeys(issue.getEpicLink());
		
		try {
			Thread.sleep(1000);
			
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
		epicLink.sendKeys(Keys.RETURN);
		
		regularExpression.sendKeys(issue.getRegularExpression());
		createIssueSubmit.click();
		
		subWindowHandler = null;
		
		driver.switchTo().window(parentWindowHandler); // switch to parent window
		
		// click the link's partial text which takes us to the issue details page
		wait = new WebDriverWait(driver, 10);	
		WebElement linkIssueCreated = wait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText("TST-")));		
		linkIssueCreated.click();

		driver.switchTo().window(parentWindowHandler);  // switch back to parent window
		
		return PageFactory.initElements(driver, IssueDetailsPage.class);
		
		
	}
	

}
