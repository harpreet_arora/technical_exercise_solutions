/**
 * The LoginPage PageObject logs the user to the system dashboard
 * The login() methods takes userid and password as the inputs and logs the user in
 */

package com.atlassian.jira.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.*;

public class LoginPage {

	WebDriver driver;
	@FindBy(how=How.ID, using="username")
	WebElement username;
	
	@FindBy(how=How.ID, using="password")
	WebElement password;
	
	@FindBy(how=How.ID, using="login-submit")
	WebElement logIn;
	
	public LoginPage(WebDriver driver)
	{
		this.driver=driver;
		driver.get("https://id.atlassian.com/login?continue=https%3A%2F%2Fjira.atlassian.com%2Fsecure%2FDashboard.jspa&application=jac");
	}
	
	public SystemDashboardPage login(String uname, String pass)
	{
		username.sendKeys(uname);
		password.sendKeys(pass);
		logIn.click();
		return PageFactory.initElements(driver, SystemDashboardPage.class);
	}
	
}
